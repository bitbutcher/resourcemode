{omit} = require 'underscore'
auth = require 'basic-auth'
{resource} = require('config').authmode
tokens = require './tokens'

module.exports =

  token:

    middleware: (req, res, next) ->
      { name, pass } = auth(req)
      return next() if name == resource.identifier and pass == resource.secret
      res.set 'WWW-Authenticate': 'Basic realm="tokens"'
      res.sendStatus(401)

    put: (req, res, next) ->
      tokens.put req.params.token, req.body, (err, value) ->
        res.type 'text/plain'
        res.send 'Token Accepted'

    delete: (req, res, next) ->
      tokens.delete req.params.token, (err, deleted) ->
        return next status: 404 unless deleted
        res.type 'text/plain'
        res.send 'Token Deleted'
