module.exports =

  access: require './access'

  identity: require './identity'

  middleware: require './middleware'

  tokens: require './tokens'
