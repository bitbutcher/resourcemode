{extend} = require 'underscore'
tokens = require './tokens'

module.exports = (overrides={}) ->
  options = extend {
    anonymous: false
    optional: false
    access: 'access'
  }, overrides
  (req, res, next) ->
    authz_failure = (error, description) ->
      return next() if options.optional
      res.setHeader 'WWW-Authenticate', 'Bearer'
      res.json 401, error: error, error_description: description
    authorization = req.get 'Authorization'
    return authz_failure 'invalid_request', 'Authorization Required' unless authorization?
    try
      [ type, token ] = authorization.split ' ', 2
    catch e
      return authz_failure 'invalid_request', 'Authorization Malformed'
    return authz_failure 'invalid_request', 'Unsupported Authorization Scheme' unless type == 'Bearer'
    tokens.get token, (error, access) ->
      return authz_failure 'invalid_token', 'Access Token Invalid or Expired' unless access?
      if access.anonymous and not options.anonymous
        return authz_failure 'invalid_token', 'Anonymous Access Not Supported'
      res.locals[options.access] = access
      next()


