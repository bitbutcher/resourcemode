{ANONYMOUS} = require './identity'
authmode = require('config').authmode
{Client} = require 'memjs'
request = require 'superagent'

client = Client.create null

key_for = (token) ->
  "access-token:#{token}"

fetch_access = (token, callback, retry=0, retries=2) ->
  return callback null if retry > retries
  req = request.get "#{authmode.server}/access/tokens/#{token}"
  req.auth authmode.resource.identifier, authmode.resource.secret
  req.on 'error', (error) ->
    console.log 'Error on token retrieval: %j', error
    fetch_access token, callback, retry + 1, retries
  req.end (res) ->
    return callback null if res.notFound
    return fetch_access token, callback, retry + 1, retries unless res.ok
    callback res.body

module.exports =

  get: (token, callback) ->
    client.get key_for(token), (error, value) ->
      return callback(error, value) if error?
      return callback error, JSON.parse(value) if value?
      fetch_access token, (access) ->
        return callback error, access unless access?
        after_put = (error, value) ->
          callback error, JSON.parse(value)
        module.exports.put token, access, after_put, 0

  put: (token, access, callback) ->
    expires_in = access.expires_in
    delete access.expires_in
    access.anonymous = access.identity == ANONYMOUS
    value = JSON.stringify(access)
    client.set key_for(token), value, 
      (error, added) ->
        callback error, if added then value else null
      parseInt(expires_in, 10)

  delete: (token, callback) ->
    client.delete key_for(token), callback
