// Generated by CoffeeScript 1.8.0
module.exports = {
  access: require('./access'),
  identity: require('./identity'),
  middleware: require('./middleware'),
  tokens: require('./tokens')
};
